module.exports = {
  publicPath: '',
  lintOnSave: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
        @import "@/styles/variables.scss";
        @import "@/styles/global.scss";
        `
      }
    }
  }
};